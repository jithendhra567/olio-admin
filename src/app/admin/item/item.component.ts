import { AddonComponent } from './../addon/addon.component';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup,Validators,FormControl, FormBuilder } from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component,Inject,OnInit} from '@angular/core';
import {MatChipInputEvent} from '@angular/material/chips';
import { AngularFirestore } from '@angular/fire/firestore';

export interface TAG {
  name: string;
}

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  item: FormGroup;
  data = {};
  addons=[];
  constructor(private _formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<ItemComponent>,
              private dialog: MatDialog,
              private db: AngularFirestore,
              @Inject(MAT_DIALOG_DATA) data){
              this.data=data;

  }

  ngOnInit(): void {
    this.item = this._formBuilder.group({
      title: ['', Validators.required],
      id: new FormControl({value:"",disabled:false}),
      img: ['',Validators.required],
      veg: ['',Validators.required],
      prize: ['',Validators.required],
      half: [false],
      hide: [false],
    });
    if(this.data['isedit']){
      this.item.patchValue(
        {
          id:this.data['id'],
          img:this.data['img'],
          title:this.data['title'],
          veg:this.data['veg'],
          prize:this.data['prize'],
          hide:this.data['hide'],
          half:this.data['half']
        }
      );
      this.tags=this.data['tags'];
      this.addons=this.data['addons'];
    }
    else
      this.item.patchValue({id:this.data['id']});
  }


  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  tags: TAG[] = [
    {name: 'Best'},
  ];

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.tags.push({name: value.trim()});
    }

    if (input) {
      input.value = '';
    }
  }

  remove(tag: TAG): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  save(){
    var data = this.item.value;
    data['tags']=this.tags;
    data['addons'] = this.addons;
    console.log(data);

    this.dialogRef.close(data)
  }

  cancel(){
    this.dialogRef.close({iscancel:true});
  }

  createaddon(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data={
      id: this.data['id']+"A"+(this.addons.length+1),
      isedit:false
    }
    var ref = this.dialog.open(AddonComponent,dialogConfig);
    ref.afterClosed().subscribe(data => {
      if(!data['iscancel'])
      this.addons.push(data);
    });

  }

  openaddons(data:any){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    data['isedit']=true;
    dialogConfig.data=data;
    var ref = this.dialog.open(AddonComponent,dialogConfig);
    ref.afterClosed().subscribe(data => {
      if(!data['iscancel']){
        var i = parseInt(data['id'].split(/(?=[A-Z])/)[2].substr(1));
        this.addons[i-1]=data;
      }
    });
  }

}
