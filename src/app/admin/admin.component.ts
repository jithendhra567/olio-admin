import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor(private db: AngularFirestore, private route: Router) {
    this.db.collection('admin').doc('pass').get().toPromise().then((data) => {
      const pass = data.data()['pass'];
      const ch = sessionStorage.getItem('user');
      if (ch != pass) { this.route.navigateByUrl('/login'); }
    });
  }

  ngOnInit(): void {
  }

}
