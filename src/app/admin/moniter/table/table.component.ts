import { MatTableDataSource } from '@angular/material/table';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CreateTableComponent } from './../create-table/create-table.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

export interface TABLES {
  id: string;
  title: string;
  isfull: string;
  prize: number;
  count: number;
  total: number;
}
var ELEMENT_DATA: TABLES[] = [

];

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  table: FormGroup;
  displayedColumns: string[] = ['id', 'title','isfull','prize','count','total'];
  id = '';
  hotel = '';
  tablesinfo = [];
  orders = new MatTableDataSource(ELEMENT_DATA)
  constructor(public _formBuilder:FormBuilder,public db:AngularFirestore,
    public dialogRef:MatDialogRef<CreateTableComponent>,
    @Inject(MAT_DIALOG_DATA) data){
      this.id = data['id'];
      this.tablesinfo=data['tablesinfo'];
      ELEMENT_DATA = [];
      var x = [];
      x = this.tablesinfo[parseInt(this.id)-1]['orders'];
      x.forEach(ele =>{
        var p = ele.prize/ele.count;
        ELEMENT_DATA.push({
          id:ele.id,title:ele.title,isfull:ele.isfull,prize:p,count:ele.count,total:ele.prize
        });
      })
      this.orders = new MatTableDataSource(ELEMENT_DATA);
      this.hotel = window.location.toString().split("/")[6];
  }

  ngOnInit(): void {
    this.table = this._formBuilder.group({
      name: new FormControl({value:"",disabled:false}),
      id: new FormControl({value:"",disabled:false}),
      payments: new FormControl({value:"",disabled:false}),
      active: new FormControl({value:"",disabled:false}),
    });

    var i = parseInt(this.id)-1;

    this.table.patchValue({
      id:this.tablesinfo[i]['id'],
      name: this.tablesinfo[i]['name'],
      payments: this.tablesinfo[i]['payments'],
      active: this.tablesinfo[i]['active'],
    });

  }

  save(){
    var i = parseInt(this.id)-1;
    this.tablesinfo[i]=this.table.value;
    this.tablesinfo[i]['orderitems'] = 0;
    this.db.collection("hotels").doc(this.hotel).set({tablesinfo:this.tablesinfo},{merge:true})
    .then(() => console.log("Saved"));
    this.dialogRef.close();
  }

  cancel(){
    this.dialogRef.close();
  }

}
