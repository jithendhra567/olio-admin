import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  items = [];
  hotelname = '';
  hoteldata = {}
  constructor(public route: ActivatedRoute,public db: AngularFirestore,public router:Router) {
    this.hotelname  = route.snapshot.params.hotel;
    this.getdata()
  }

  getdata(){
    var data = {};
    this.db.collection(this.hotelname).valueChanges().subscribe(val=>{
      val.forEach(data => this.items.push(data))
    });
    return data;
  }


  open(menu){
    sessionStorage.setItem("menu",JSON.stringify(menu));
  }

  ngOnInit(): void {
  }

}
