import { TableComponent } from './table/table.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { table } from 'console';
import { CreateTableComponent } from './create-table/create-table.component';

export interface TABLES {
  id: number;
  name: string;
  payments:number;
  orderitems:number;
  active: boolean;
}
var ELEMENT_DATA: TABLES[] = [

];

@Component({
  selector: 'app-moniter',
  templateUrl: './moniter.component.html',
  styleUrls: ['./moniter.component.scss']
})
export class MoniterComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name','payments','active','orderitems','action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  hotel = '';
  totaltables = 0;
  tablesinfo = [];
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private bottomSheet: MatBottomSheet,public route: ActivatedRoute,public dialog:MatDialog,
    public db:AngularFirestore) {
    this.hotel = route.snapshot.params.hotel;
    this.getTableData();
  }



  getTableData(){
    this.db.collection("hotels").doc(this.hotel).valueChanges().subscribe(val=>{
      this.totaltables=val['tables'];
      ELEMENT_DATA=[];
      val['tablesinfo'].forEach(info => {
        ELEMENT_DATA.push({
          name:info["name"],id:info["id"],payments:info["payments"],active:info['active'],orderitems:info['orderitems'],
        });
      });
      this.tablesinfo = val['tablesinfo'];
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
    })

  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

  create(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      id: this.totaltables+1,
      tablesinfo: this.tablesinfo
    }
    var ref = this.dialog.open(CreateTableComponent,dialogConfig);
    ref.afterClosed().subscribe(data => {

    });
  }

  opentable(id){

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id: id,
      tablesinfo: this.tablesinfo
    }
    var ref = this.dialog.open(TableComponent,dialogConfig);
    ref.afterClosed().subscribe(data => {
    });
  }

}
