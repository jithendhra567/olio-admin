import { MatSnackBar } from '@angular/material/snack-bar';
import { ItemComponent } from './../../item/item.component';
import { MatDialog, MatDialogConfig, MatDialogModule } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

export interface ITEM {
  id: number;
  title: string;
  prize:number;
  hide: boolean;
}
var ELEMENT_DATA: ITEM[] = [

];

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  displayedColumns: string[] = ['id', 'title','prize','hide'];
  dataSource:any;
  menu: FormGroup;
  data = {};
  hotel = '';
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(public _formBuilder: FormBuilder,public db: AngularFirestore,public route:ActivatedRoute,public dialog: MatDialog,private snackBar: MatSnackBar) {
    this.data=JSON.parse(sessionStorage.getItem("menu"));
    this.update();
    this.hotel  = window.location.toString().split('/')[6]
  }

  update(){
    ELEMENT_DATA=[];
    this.data['menu'].forEach(element => {
      ELEMENT_DATA.push({
        id:element['id'],title:element['title'],prize:element['prize'],hide:element['hide']
      });
    });
    this.dataSource = new MatTableDataSource(ELEMENT_DATA);
  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.menu = this._formBuilder.group({
      title: ['', Validators.required],
      id: new FormControl({value:"",disabled:false}),
      img: ['',Validators.required],
      veg: ['',Validators.required]
    });

    this.menu.patchValue({
      id: this.data['id'],
      img: this.data['img'],
      veg: this.data['veg'],
      title: this.data['title']
    });
  }

  save(): void {
    this.data['title'] = this.menu.value.title;
    this.data['img'] = this.menu.value.img;
    this.data['veg'] = this.menu.value.veg;

    this.db.collection(this.hotel).doc(this.data['id']).set(this.data,{merge:true})
    .then(() => this.snackBar.open('Saved', this.data['id'], {
      duration: 2000,
    }))
  }

  hide(id){
    var i = parseInt(id.split("I")[1])-1;
    this.data['menu'][i]['hide']=!this.data['menu'][i]['hide'];
    this.update();
  }

  openitem(data){
    var i = parseInt(data['id'].split(/(?=[A-Z])/)[1].substr(1));
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data=this.data['menu'][i-1];
    dialogConfig.data['isedit']=true;
    var ref = this.dialog.open(ItemComponent,dialogConfig);
    ref.afterClosed().subscribe(data => {
      if(!data['iscancel']){
        this.data['menu'][i-1]=data;
        this.update();
      }
    });
  }

}
