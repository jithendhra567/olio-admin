import { Component, Inject, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-table',
  templateUrl: './create-table.component.html',
  styleUrls: ['./create-table.component.scss']
})
export class CreateTableComponent implements OnInit {

  table: FormGroup;
  id = '';
  hotel = '';
  tablesinfo = [];
  constructor(public _formBuilder:FormBuilder,public db:AngularFirestore,
    public dialogRef:MatDialogRef<CreateTableComponent>,
    @Inject(MAT_DIALOG_DATA) data){
      this.id = data['id'];
      this.tablesinfo=data['tablesinfo'];
      this.hotel = window.location.toString().split("/")[6];
  }

  ngOnInit(): void {
    this.table = this._formBuilder.group({
      name: new FormControl({value:"",disabled:true}),
      id: new FormControl({value:"",disabled:true}),
      payments: new FormControl({value:"",disabled:true}),
      active: new FormControl({value:"",disabled:true}),
    });
    this.table.patchValue({
      id:this.id,
      name: "not occupied",
      payments: "nopayments",
      active: "inactive",
    });
  }

  save(){
    var x  = this.table.value;
    x['orderitems'] = 0;
    this.tablesinfo.push(x);
    this.db.collection("hotels").doc(this.hotel).set({tables:this.id,tablesinfo:this.tablesinfo},{merge:true})
    .then(() => console.log("Created"));
    this.dialogRef.close();
  }

  cancel(){
    this.dialogRef.close({iscancel:true});
  }

}
