import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet'
import { createComponent } from '@angular/compiler/src/core';
import { CreateComponent } from '../create/create.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { callbackify } from 'util';
import { Router } from '@angular/router';
export interface Hotel {
  title: string;
  id: string;
  url: string;
  categories:string;
  tables: string;
}

var ELEMENT_DATA: Hotel[] = [
];


@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.scss']
})
export class HotelsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'title', 'url','categories','tables','action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private bottomSheet: MatBottomSheet,public db: AngularFirestore,public router: Router) {
    this.updatetable();
  }

  updatetable(){
    this.db.collection("hotels").get()
    .toPromise()
    .then((data)=>{
      data.forEach(item=>{
        var info = item.data();
        ELEMENT_DATA.push({
          title:info["title"],id:info["id"],url:info["url"],categories:info["categories"],tables:info["tables"]
        });
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);

      })
      ELEMENT_DATA=[];
    })

  }


  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

  open(id,title): void {
    var ref = this.bottomSheet.open(CreateComponent,
      {
        panelClass: 'custom-width',
        data:{id:id,title:title}
      });
    document.getElementById("overlay").style.display="flex";
    ref.afterDismissed().subscribe(() => {
      this.updatetable();
      console.log('Bottom sheet has been dismissed.');
      document.getElementById("overlay").style.display="none";
    });
  }

}
