import { element } from 'protractor';
import { ItemComponent } from './../item/item.component';
import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {MatDialog, MatDialogConfig, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatStepper } from '@angular/material/stepper';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})


export class CreateComponent implements OnInit {

  hotel: FormGroup;
  menu: FormGroup;

  current_hotel_name = '';
  current_hotel_id = '';
  current_menu_id='';
  menudata = [];
  hotel_data = [];
  itemdata=[];
  isedit=false;
  constructor(public _formBuilder: FormBuilder,
              public sheet: MatBottomSheet,
              public dialog: MatDialog,
              private snackBar: MatSnackBar,
              public db: AngularFirestore,
              @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
                if(data.id!='') {
                  this.current_hotel_id=data.id;
                  this.current_hotel_name=data.title;
                  this.isedit=true;
                }
  }

  close(){
    this.sheet.dismiss();
  }

  ngOnInit() {
    this.hotel = this._formBuilder.group({
      title: ['',Validators.required],
      id: new FormControl({value:"id",disabled:true}),
      url: new FormControl({value:"https://olio-ordering.web.app/",disabled:true}),
    });
    if(this.isedit)
    this.hotel.patchValue({
      id:this.current_hotel_id,
      title:this.current_hotel_name,
      url:"https://olio-ordering.web.app/"+this.current_hotel_id
    });

    this.menu = this._formBuilder.group({
      title: ['', Validators.required],
      id: new FormControl({value:"id",disabled:false}),
      img: ['',Validators.required],
      veg: ['',Validators.required]
    });

  }

  next(stepper: MatStepper){
    this.db.collection(this.current_hotel_id)
    .get()
    .toPromise()
    .then((data)=> data.forEach((item)=> this.menudata.push(item.data())))
    stepper.next();
  }

  createitem(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data={
      id: this.current_menu_id+"I"+(this.itemdata.length+1),
      isedit:false
    }
    var ref = this.dialog.open(ItemComponent,dialogConfig);
    ref.afterClosed().subscribe(data => {
      if(!data['iscancel'])
      this.itemdata.push(data);
    });

  }

  openitem(step:MatStepper,data:any){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    data['isedit']=true;
    dialogConfig.data=data;

    var ref = this.dialog.open(ItemComponent,dialogConfig);
    ref.afterClosed().subscribe(data => {
      if(!data['iscancel']){
        var i = parseInt(data['id'].split(/(?=[A-Z])/)[1].substr(1));
        this.itemdata[i-1]=data;
      }
    });

  }

  createmenu(stepper: MatStepper){
    this.current_menu_id="H"+(this.menudata.length+1);
    this.menu.patchValue({id:this.current_menu_id});
    stepper.next();
  }

  openmenu(step:MatStepper,data:any){
    this.current_menu_id=data.id;

    this.menu.patchValue({
      id:data['id'],
      title:data['title'],
      img: data['img'],
      veg: data['veg']
    });
    this.itemdata=data['menu'];
    step.next();
  }

  //backend
  onkey(id: HTMLElement,name: HTMLElement,url: HTMLElement){
    var x = this.generateId(name['value'])
    id['value']=x;
    url['value']="https://olio-ordering.web.app/"+x;
    this.current_hotel_id=id['value'];
    this.current_hotel_name=name['value'];
  }
  nothing(){};


  generateId(name:string){
    var id: string = "";
    name.split(" ").forEach(s=> id=id+s.toUpperCase().charAt(0))
    return id+Date.now();
  }

  save(stepper: MatStepper){
    if(this.menu.status=="VALID"){
      var x = this.menu.value;

      x['menu']=this.itemdata;
      x['items']=this.itemdata.length+1;
      var i = parseInt(this.current_menu_id.substr(1));
      if(this.menudata.length>=i) this.menudata[i-1]=x;
      else this.menudata.push(x);


      this.db.collection("hotels").doc(this.current_hotel_id)
      .set({
        id:this.current_hotel_id,
        title:this.current_hotel_name,
        url:"https://olio-ordering.web.app/"+this.current_hotel_id,
        tables:0,
        categories:this.menudata.length
      });

      this.hotel_data.push(x);
      this.db.collection(this.current_hotel_id)
      .doc(this.current_menu_id)
      .set(x,{merge:true}).then(() => {
        this.snackBar.open('Saved', 'Add another', {
          duration: 2000,
        });
        stepper.previous();
      })
      .catch(() => {
        this.snackBar.open('Please Check your Connection', 'Try Again', {
          duration: 2000,
        });
      })
    }
  }

}
