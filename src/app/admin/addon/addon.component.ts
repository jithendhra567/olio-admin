import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-addon',
  templateUrl: './addon.component.html',
  styleUrls: ['./addon.component.scss']
})
export class AddonComponent implements OnInit {

  data={};
  addon:FormGroup
  constructor(private _formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddonComponent>,
    @Inject(MAT_DIALOG_DATA) data){
    this.data=data;
  }


  ngOnInit(): void {
    this.addon = this._formBuilder.group({
      title: ['', Validators.required],
      id: new FormControl({value:"",disabled:false}),
      img: ['',Validators.required],
      prize: ['',Validators.required],
    });
    if(this.data['isedit']){
      this.addon.patchValue(
        {
          id:this.data['id'],
          img:this.data['img'],
          title:this.data['title'],
          prize:this.data['prize'],
        }
      );
    }
    else
      this.addon.patchValue({id:this.data['id']});
  }

  save(){
    var data = this.addon.value;
    this.dialogRef.close(data)
  }

  cancel(){
    this.dialogRef.close({iscancel:true});
  }

}
