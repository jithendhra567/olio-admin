import { MenuComponent } from './moniter/menu/menu.component';
import { CategoriesComponent } from './moniter/categories/categories.component';
import { MoniterComponent } from './moniter/moniter.component';
import { AdminComponent } from './admin.component';
import { HotelsComponent } from './hotels/hotels.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:'',pathMatch:'full',redirectTo:'dashboard/hotel'},
  {
    path: 'dashboard',
    component: AdminComponent,
    children: [
      {path: 'hotel', component:HotelsComponent},
      {
        path: 'moniter/:hotel',
        component:MoniterComponent,
        children: [
          {path: '' , component:CategoriesComponent},
          {path: 'menu', component:MenuComponent}
        ]
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

