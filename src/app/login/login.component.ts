import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private db: AngularFirestore, private snackBar: MatSnackBar, private route: Router){

  }
  loading = false;
  email = '';
  pass = '';

  // tslint:disable-next-line: typedef
  ngOnInit(){

  }

  // tslint:disable-next-line: typedef
  login() {
    this.loading = true;

    if (this.email == 'admin'){

      this.db.collection('admin').doc('pass').get().toPromise().then((data) => {
        const pass = data.data()['pass'];
        if (this.pass == pass) {
          sessionStorage.setItem('user', pass);
          this.route.navigateByUrl('/admin');
        }
        else { this.snackBar.open('Incorrect Login', 'Try Again', {
          duration: 2000,
        });
        }
      }).catch(err => this.snackBar.open('Check your Internet Connection', 'Try Again', {
        duration: 2000,
      }));
    }
    else { this.snackBar.open('Incorrect Login', 'Try Again', {
      duration: 2000,
    });
    }
    setTimeout(() => {
      this.loading = false;
    }, 1500);
  }
}
